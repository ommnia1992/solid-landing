/**
 * URL param
 * @description Get named url param value if exist
 * @param {string} param - Name of parameter
 * @return {string} - value of searching param
 */

export default function (param) {
  let urlParam = ''
  let sPageURL = decodeURIComponent(window.location.search.substring(1))
  let sURLVariables = sPageURL.split('&')
  let sParameterName
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=')
    if (sParameterName[0] === param) {
      urlParam = sParameterName[1] === undefined ? '' : sParameterName[1]
    }
  }
  if (!(urlParam.length > 0)) {
    if (param === 'utm_source') {
      urlParam = window.location.hostname ? window.location.hostname : 'unknown'
    } else {
      urlParam = ''
    }

  }
  return urlParam
}
