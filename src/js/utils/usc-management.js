/**
 * UTM source cookie management
 * @description Special logic for landing Alisa
 */
import urlParam from './url_param'
import Cookies from 'js-cookie'

export default function (param) {
  // 0. Config
  let cookieName = param
  let cookieLife = 2 // 1 day
  // 1. If param value exists set to cookie, check localhost fallback
  let paramValue = urlParam(param)
  // 2. If param value exists (without host name as fallback)
  if (paramValue.length > 0) {
    if (paramValue !== window.location.hostname) {
      // 3. If cookie already exists delete it
      if (Cookies.get(cookieName)) {
        Cookies.remove(cookieName);
      }
      // 4. Set the utm_source_rt cookie
      Cookies.set(cookieName, paramValue, { expires: cookieLife });
    }
  }
}
