/**
 * Solid main JS
 * 0. Init
 */

// 0. Init ---------------------------------------------------------------------
import $ from 'jquery'
import svg4everybody from 'svg4everybody'
import USCManagement from './utils/usc-management'

$(document).ready(function () {

  // Init svg4everybody
  svg4everybody()

  // Accordion
  let accBtn = $('.accordion-header')
  let accBody = $('.accordion-body')
  accBody.hide()
  accBtn.on('click', function () {
    if(accBody.is(':visible')) {
      accBody.slideUp(400)
    }
    if ($(this).next(accBody).is(':visible')) {
      $(this).next(accBody).slideUp(400)
    }else {
      $(this).next(accBody).slideDown(400)
    }
  })


  // 13. Marketing features -------------------------------------------- [final]
  $('[data-mf]').on('click', (e) => {
    // console.log($(e.target).attr('data-mf'))
    // Description:
    // Use data attribute 'data-mf' with target value on clickable element
    if ('reachGoal' in window.yaCounter53681812) {
      window.yaCounter53681812.reachGoal($(e.target).attr('data-mf'))
    }
    if (typeof window.gtag === 'function') {
      window.gtag('event', 'click', {
        'event_category': $(e.target).attr('data-mf')
      })
    }
  })

  // 14. UTM source cookie management ---------------------------------- [final]
  USCManagement('utm_source')
  USCManagement('utm_medium')
  USCManagement('utm_campaign')
  USCManagement('type')
  USCManagement('source')
  USCManagement('block')
  USCManagement('position')
  USCManagement('utm_content')
  USCManagement('utm_term')

})
